-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2014 at 09:29 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `diary`
--

-- --------------------------------------------------------

--
-- Table structure for table `boards`
--

CREATE TABLE IF NOT EXISTS `boards` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(120) NOT NULL,
  `owner_id` int(255) NOT NULL,
  `public` int(1) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `diary`
--

CREATE TABLE IF NOT EXISTS `diary` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `date` datetime NOT NULL,
  `title` varchar(120) NOT NULL,
  `desc` text NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `compleat` int(1) DEFAULT '0',
  `compleation_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `it_staff`
--

CREATE TABLE IF NOT EXISTS `it_staff` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rank` int(1) NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `it_staff`
--

INSERT INTO `it_staff` (`id`, `user_id`, `rank`) VALUES
(1, 23, 3);

-- --------------------------------------------------------

--
-- Table structure for table `it_support`
--

CREATE TABLE IF NOT EXISTS `it_support` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `assigned_user` int(255) NOT NULL,
  `creation_date` date NOT NULL,
  `title` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `compleated` int(1) NOT NULL DEFAULT '0',
  `compleation_date` date DEFAULT NULL,
  `reminder_set` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `it_support`
--

INSERT INTO `it_support` (`id`, `assigned_user`, `creation_date`, `title`, `desc`, `compleated`, `compleation_date`, `reminder_set`) VALUES
(1, 23, '2014-07-14', 'test Object 1', 'Nothing to do but test this shit', 0, '2014-07-31', 1),
(12, 23, '2014-07-16', 'code  test', 'desc', 0, NULL, 0),
(13, 23, '2014-07-16', 'code  test', 'desc', 0, '0000-00-00', 0),
(14, 23, '2014-07-16', 'code  testwwww', 'desc', 0, '0000-00-00', 0),
(15, 23, '2014-07-16', 'project tango 3d real world phone modeling ', 'desc', 0, '0000-00-00', 0),
(16, 23, '1970-01-01', 'dsdsd', 'desc', 0, '0000-00-00', 0),
(17, 23, '1970-01-01', 'ddd', 'desc', 0, '0000-00-00', 0),
(18, 23, '2014-07-16', 'test!aa', 'desc', 0, '0000-00-00', 0),
(19, 23, '2014-07-16', 'Apada', 'desc', 0, '0000-00-00', 0),
(20, 23, '2014-07-16', 'adas', 'desc', 0, '0000-00-00', 0),
(21, 23, '2014-07-16', 'asda 21', 'desc', 0, '0000-00-00', 0),
(22, 23, '2014-07-16', 'test!', 'desc', 0, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `support_notes`
--

CREATE TABLE IF NOT EXISTS `support_notes` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `date` date NOT NULL,
  `author` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trello`
--

CREATE TABLE IF NOT EXISTS `trello` (
  `id` int(255) NOT NULL,
  `assigned_user` int(255) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(120) NOT NULL,
  `desc` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `compleat` int(1) NOT NULL DEFAULT '0',
  `compleation_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pwd` varchar(128) NOT NULL,
  `rank` int(1) NOT NULL DEFAULT '0',
  `status` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `security_hash` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `pwd`, `rank`, `status`, `date`, `security_hash`) VALUES
(23, 'Barry', 'barry.smith@mcewanfraserlegal.co.uk', '$2y$12$K4TYtkry9QXKHlgymdKB4eHrQ3stAvJhAz3.ccmSyWCDuEXlU6bWW', 1, 'active', '2014-06-22', 'b6589fc6ab0dc82cf12099d1c2d40ab994e8410c');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
