 <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $indexPage; ?>">Iris Work Diary</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo $indexPage; ?>"><i class="fa fa-home"></i> Home</a></li>
                    <li><a href="#about"><i class="fa fa-info-circle"></i> About</a>
                    </li>
                    <li><a href="#reminder"><i class="fa fa-clock-o"></i> Set Reminder</a></li>
                    <li><a href="#contact"><i class="fa fa-edit"></i> Contact</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav account">
                    
                    <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear"></i> <?php echo $_SESSION['iris_email']; ?><b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li>
                              <a href="profile.php" title="Edit your Profile"><i class="fa fa-user"></i> Profile</a>
                            </li>
                             
                            <li>
                            <a href="logout.php" title="Log off from here"><i class="fa fa-sign-out"></i> Logout</a></li>
                          </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>