<?php
// site functions

function count_open_support($dbcon, $userID){
	$result = $dbcon->query("SELECT * FROM it_support WHERE compleated='0' AND assigned_user='$userID'");
    $openNum = $result->num_rows;
    if($openNum>0){
    	$badgeClass='redBadge';
    }else{
    	$badgeClass='greenBadge';
    }
    $numResult='<span class="'.$badgeClass.' badge">'.$openNum.'</span>';

    return $numResult;
    mysqli_close($dbcon);
}
function count_open_trello($dbcon, $userID){
	if($result = $dbcon->query("SELECT * FROM trello WHERE compleated='0' AND assigned_user='$userID'"))
	{
    	$openNum = $result->num_rows;
    }else{
    	$openNum='0';
    }
    if($openNum>0){
    	$badgeClass='redBadge';
    }else{
    	$badgeClass='greenBadge';
    }
    $numResult='<span class="'.$badgeClass.' badge">'.$openNum.'</span>';

    return $numResult;
    mysqli_close($dbcon);
}
function support_roster_check(){
    $today = date('l');
    switch ($today) {
        case 'Monday':
            return 'Today is Yan\'s turn to man the IT Help Desk';
            break;
        case 'Tuesday':
            return 'Today is Barry\'s turn to man the IT Help Desk';
            break;
        case 'Wednesday':
            return 'Today is Jack\'s turn to man the IT Help Desk';
            break;
        case 'Thursday':
           return 'Today is Barry\'s turn to man the IT Help Desk';
            break;
        case 'Friday':
            return 'Today is Jack\'s turn to man the IT Help Desk';
            break;
        default:
             return 'The Help Desk Is Currently Closed';
            break;
    }
}
function get_user_dropDown($dbcon){
    $userList='';
    $result = $dbcon->query("SELECT * FROM `users`");
    while($row = $result->fetch_assoc()){

        if($row['id']==$_SESSION['iris_user_id']){
            $selectBlock='selected="selected"';
        }else{
            $selectBlock='';
        }

        $userList.='<option value="'.$row['id'].'" '.$selectBlock.'>'.$row['name'].'</option>';
    }
    return $userList;
}

function db_date_format($data) {
    
    $newdate = str_replace("/", "-", $data);
    $newDay = date('Y-m-d H:i:s', strtotime($newdate));
    $newDay = strtolower($newDay);
    return $newDay;
    
}

function user_date_format($data) {
    
    $newdate = str_replace("/", "-", $data);
    $newDay = date('d-m-Y', strtotime($newdate));
    $newDay = strtolower($newDay);
    return $newDay;
    
}

function get_userName($dbcon, $id){
    $result = $dbcon->query("SELECT * FROM users WHERE id='$id'");
    $row = $result->fetch_assoc();

    $name = $row['name'];
    return $name;
    mysqli_close($dbcon);
}
?>