<?php 
session_start();
include('inc/db-config.php');
include('inc/functions.php');
if(!isset($_SESSION['user_id'])){
  header('location: index.php?error=login');
}
$errors = array();


?>
<!doctype html>
<html>
    
    <head>
        <title><?php echo $siteTitle; ?></title>
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" type="text/javascript"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- meta here -->
    </head>
    
    <body>
<div class="container">
    <div class="row">
        <p align="center">
            <h2>Hello <?php echo $_SESSION['name'].'!'; ?></h2>
            <p class="lead">Thank you for logging in!</p>
            <p><a href="logout.php">Log out here</a></p>
       </p>
    </div>
    <div align="center">

    </div>

</div><!-- /container -->
    </body>

</html>