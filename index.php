<?php 
session_start();
include('inc/db-config.php');
include('inc/functions.php');
if(isset($_SESSION['iris_user_id'])){
  header('location: main.php');
}
$errors = array();
// Get user Information
   
if(isset($_POST['submitBTN'])){
    
    if(empty($_POST['pwd'])){
        
        $errors[] ='<div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        Please enter a password.</div>';
        mysqli_close($dbcon);
    }
    
    if(empty($_POST['email'])){
        
        $errors[] ='<div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        We need your Email address.</div>';
        mysqli_close($dbcon);
    }
    
    if(empty($errors))
    {
        $email = mysqli_real_escape_string($dbcon, $_POST['email']);  
        $pwd = mysqli_real_escape_string($dbcon, $_POST['pwd']);          
    
        $result = $dbcon->query("SELECT * FROM users WHERE email='$email'");
        
        if($result->num_rows){

            $row = $result->fetch_assoc();
            $db_pwd = $row['pwd'];

            if(password_verify($pwd, $db_pwd))
            {
                  
                    $_SESSION['name'] = $row['name'];
                    $_SESSION['user_id'] = $row['id'];                   

                    // redirect to display page
                    
                    if ($row["rank"] > 0)
                    {
                        $_SESSION['admin'] = $row['id'];
                       
                    }

                    header('location: main.php');
                                
            }else{
                // wrong password
               
                $errors[] ='<div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    Wrong password, try again!</div>';
                    mysqli_close($dbcon);
            }
        }else{
            
            $errors[] ='<div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    Wrong Email or address not found, try again!</div>';
                    mysqli_close($dbcon);
           
        }
    }
}
        


?>
<!doctype html>
<html>
    
    <head>
        <title><?php echo $siteTitle; ?></title>
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" type="text/javascript"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- meta here -->
    </head>
    
    <body>
<div class="container">
    <div class="row">
        <p align="center">
       </p>
    </div>
            <form class="form-signin" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <h2 class="form-signin-heading">Sign in</h2> <?php 
            if(empty($errors) === false)
            {
                echo '<ul class="error">';
                
                foreach ($errors as $error)
                {
                    echo "<li>{$error}</li>";
                }
                echo '</ul>';
            }
        ?>
                <input type="email" class="form-control" placeholder="Email address" name="email" required>
                <input type="password" class="form-control" placeholder="Password" name="pwd" required>
                <p><button class="btn btn-lg btn-primary btn-block" type="submit" name="submitBTN">Sign in</button></p>
            </form>
            <p align="center">Not registered? <a href="register.php" title="Register now">Register now</a></p>
        </div><!-- /container -->
    </body>

</html>