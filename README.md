# Bcrypt PHP Login README #

### What is this for? ###

* Quick php template using the Bcrypt password hashing algorithm. 
* Designed as rough guide only!
* Will require webserver running php 5.5.0 and higher

### How do I get set up? ###

* set up mysqlDB
* Make users table with id, name, email, pwd
* Edit Config file and enter required settings 
* Register 
* Then login

### Who do I talk to? ###

Barry Smith (@lazysod on twitter)