<?php 
session_start();
require_once('inc/db-config.php');
require_once('inc/functions.php');
if(isset($_SESSION['user_id'])){
  header('location: cpanel.php');
}
$errors = array();
//
		
//
if(isset($_POST['submitBTN'])){
    
    

        if(empty($_POST['name']))
        {
           
            $errors[] ='<div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    Please enter your name</div>';
                    mysqli_close($dbcon);  
        }
        if(empty($_POST['pwd']))
        {
            
            $errors[] ='<div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    Please enter a password</div>';
                    mysqli_close($dbcon);  
        }
        if($_POST['pwd'] != $_POST['pwd2']){
            
            $errors[] ='<div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    Passwords don\'t match</div>';
                    mysqli_close($dbcon);  
        }
        if(empty($_POST['email']))
        {
            $errors[] ='We need your Email address';
        }else{

            $email = mysqli_real_escape_string($dbcon, $_POST['email']);
        }
                
        $result = $dbcon->query("SELECT * FROM users WHERE email='$email'");
        if($result->num_rows)
        {
                
                $errors[] ='<div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    That Email address already exists!!</div>';   
                mysqli_close($dbcon);   
        }
    

        if(empty($errors))
        {             
                
                $name = mysqli_real_escape_string($dbcon, $_POST['name']);     
                $pwd = $_POST['pwd'];
                $pwdEncrypted = password_hash($pwd, PASSWORD_BCRYPT, $dbCost); 
                $today = date('Y-m-d H:i:s');
                $sec_hash = sha1($today);
                // First user check
                $query = $dbcon->query("SELECT * FROM users");
                
                if($query->num_rows<1){
                    // no users set, so first user will be added as admin
                    
                    $sql="INSERT INTO `users`(`id`, `name`, `email`, `pwd`, `rank`, `status`, `date`, `security_hash`) VALUES ('', '$name', '$email', '$pwdEncrypted', 1, 'active', '$today', '$sec_hash')";
                }else{

                    $sql="INSERT INTO `users`(`id`, `name`, `email`, `pwd`, `rank`, `status`, `date`, `security_hash`) VALUES ('', '$name', '$email', '$pwdEncrypted', 0, 'active', '$today', '$sec_hash')";
                }
                //
              
                $result = $dbcon->query($sql);
                if(mysqli_affected_rows($dbcon)>0)
                    {
                        header('location: '.$indexPage);
                    }else{
                        
                        $errors[] ='<div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    Error: Registration failure</div>';    
                     mysqli_close($dbcon);   
                    }
        }
}
//


?>
<!doctype html>
<html>
    
    <head>
        <title>Register</title>
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootswatch/3.0.0/cerulean/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" type="text/javascript"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- meta here -->
    </head>
    <body>
<div class="container">
    <p align="center">
    <?php 
        if(empty($errors) === false)
        {
            echo'<ul class="error">';
            
            foreach ($errors as $error)
            {
                echo "<li>{$error}</li>";
            }
            echo'</ul>';
        }
    ?></p>
            <form class="form-signin" method="POST" action="register.php">
                <h2 class="form-signin-heading">Register</h2>
                <div class="form-group">
                  <label class="control-label">Your Name</label>
                  <div class="controls">
                    <input name="name" type="text" required class="form-control" id="name" placeholder="Your Name">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label">Email Address</label>  
                  <div class="controls">
                    <input name="email" type="email" required class="form-control" id="email" placeholder="Email Address">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label">Password</label>                  
                  <div class="controls">
                    <input name="pwd" type="password" required class="form-control" id="pwd">
                  </div>
                  <label class="control-label">Password Confirm</label>                  
                  <div class="controls">
                    <input name="pwd2" type="password" required class="form-control" id="pwd2">
                  </div>
                </div>
                <p><button class="btn btn-lg btn-primary btn-block" type="submit" name="submitBTN">Register</button></p>
                <p align="center">Already registered? <a href="index.php" title="Sign in">Sign in!</a></p>
            </form>
        </div><!-- /container -->
    </body>

</html>